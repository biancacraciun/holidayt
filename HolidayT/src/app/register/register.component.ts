import { Component, OnInit } from '@angular/core';
 import{RegisterService}  from './register.service';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
  providers: [RegisterService]
})
export class RegisterComponent implements OnInit {

  constructor( private registersrvice: RegisterService) { }

  ngOnInit(): void {
  }
registerUser(event)
{
  var user=event.target.elements[0].value;
  var pass=event.target.elements[1].value;
  var name=event.target.elements[2].value;
  var pre=event.target.elements[3].value;
  var group=event.target.elements[4].value;
  this.registersrvice.registerUser(user,pass,name,pre,group);
}
}
