import { Component, OnInit } from '@angular/core';
import{ HService }  from './h.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  providers: [HService]
})
export class HomeComponent implements OnInit {

  constructor(private homeService: HService) { }

  ngOnInit(): void {
    this.homeService.test();
  }

}
