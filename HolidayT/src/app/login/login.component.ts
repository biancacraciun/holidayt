import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
 import {LoginService} from  './login.service'
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
   providers: [LoginService]
})
export class LoginComponent implements OnInit {

  constructor(private loginservice: LoginService) { }

  ngOnInit(): void {
  }

  loginUser(event) {
    event.preventDefault()
    console.log(event)
    var username = event.target.elements[0].value;
    var password = event.target.elements[1].value;
 this.loginservice.check(username,password);

  }
}
